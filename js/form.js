const $modalElement = $('#remove');

function remove(deleteButtonElement = {}) {
	let {dataset: {id} = {}} = deleteButtonElement;
	if (isNaN(id)) return;
	let rowSelectorId = `tr[data-id=${id}]`;
	let $tableRow = $(rowSelectorId);
	$modalElement.modal('hide');
	$tableRow.fadeOut('slow', () => {
		$tableRow.remove();
	});
};

$modalElement.on('show.bs.modal', function(event) {

	let $tableRow = $(event.relatedTarget).closest('[data-id]');

	let id = $tableRow.data('id');

	let $titleElement = $('.modal-title');

	$titleElement.text((index, text) => {
		return text = `Confirm delete #${id}`;
	});

	let $modalDeleteButton = $('.modal-action');

	$modalDeleteButton.attr('data-id', id);

});
