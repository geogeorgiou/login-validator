jQuery(function ($) {

    $('#create-user').validate({
        rules: {
            
            id: {
            required: true,
            minlength: 6,
            maxlength: 9,
            digits: true
            },

            firstname: {
            required: true
            },
            lastname: {
                required: true
            },
            email: {
            required: true,
            email: true
            }
        },
        
        messages: {

            id: {
            required: 'Enter your fuckin Password! 🤬',
            minlength: 'Password should be more than 6 digits',
            maxlength: 'Password should be maxed out at 9 digits',
            digits: 'Password should contain only digits'
            },

            email: {
                required: 'Enter your fuckin email! 📧',
            }
        }
    });
});